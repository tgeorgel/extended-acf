<?php

/**
 * Copyright (c) Vincent Klaiber & Thomas Georgel.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @see https://github.com/wordplate/extended-acf
 */

declare(strict_types=1);

namespace WordPlate\Acf;

class ConfigDefaults
{
    /** @var array */
    protected static $items = [];

    public static function has(string $key): bool
    {
        return array_key_exists($key, static::$items);
    }

    /**
     * Set a default value for the given config key
     *
     * @param string $key   Config key
     * @param mixed  $value Default value
     */
    public static function set(string $key, $value): void
    {
        static::$items[$key] = $value;
    }

    /**
     * Push array into items.
     *
     * @param array $items Associative [key => default_value]
     */
    public static function push(array $items): void
    {
        static::$items = $items + static::$items;
    }

    /**
     * Get a single key from defaults.
     *
     * @return mixed
     */
    public static function get(string $key)
    {
        return static::$items[$key] ?? null;
    }

    /**
     * Get all defaults configurations
     */
    public static function all(): array
    {
        return static::$items;
    }
}
