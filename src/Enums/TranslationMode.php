<?php

/**
 * Copyright (c) Thomas Georgel.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @see https://github.com/wordplate/extended-acf
 */

declare(strict_types=1);

namespace WordPlate\Acf\Enums;

enum TranslationMode: string
{
    case ENABLED = 'enabled'; // Translate this field translate
    case DISABLED = 'disabled'; // Do not translate
    case COPY_ONCE = 'copy_once'; // Translate, copy the value once when creating a new translation
    case SYNC = 'sync'; // Translate, and keep the value in sync between translations

    public function toWpml(): int
    {
        return match ($this) {
            self::ENABLED => 2,
            self::DISABLED => 0,
            self::COPY_ONCE => 3,
            self::SYNC => 1,
        };
    }

    public function toPolylang(): string
    {
        return match ($this) {
            self::ENABLED => 'translate',
            self::DISABLED => 'ignore',
            self::COPY_ONCE => 'copy_once',
            self::SYNC => 'sync',
        };
    }

    public static function fromWpml(int $mode): self
    {
        return match ($mode) {
            2 => self::ENABLED,
            0 => self::DISABLED,
            3 => self::COPY_ONCE,
            1 => self::SYNC,
        };
    }

    public static function fromPolylang(string $mode): self
    {
        return match ($mode) {
            'translate' => self::ENABLED,
            'ignore' => self::DISABLED,
            'copy_once' => self::COPY_ONCE,
            'sync' => self::SYNC,
        };
    }
}
