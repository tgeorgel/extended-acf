<?php

/**
 * Copyright (c) Vincent Klaiber.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @see https://github.com/wordplate/extended-acf
 */

declare(strict_types=1);

namespace WordPlate\Acf\Fields\Settings;

use WordPlate\Acf\Enums\TranslationMode;

trait Translatable
{
    /**
     * Set the translation mode for this field (Polylang and WPML are supported).
     *
     * @param TranslationMode $mode
     */
    public function translate(TranslationMode $mode = TranslationMode::ENABLED): static
    {
        $this->settings['wpml_cf_preferences'] = $mode->toWpml();
        $this->settings['translations'] = $mode->toPolylang();

        return $this;
    }
}
